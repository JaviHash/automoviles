//
//  AgregarAutomovilViewController.swift
//  Automoviles
//
//  Created by CC5-29 on 08/06/18.
//  Copyright © 2018 CC5-29. All rights reserved.
//

import UIKit
import CoreData

class AgregarAutomovilViewController: UIViewController {

    var lblTexto: String?
    var persistenContainer: NSPersistentContainer?
    var managedObjectContext: NSManagedObjectContext?
    
    @IBOutlet weak var lblStatus: UILabel!
    
    
    @IBOutlet weak var txtAño: UITextField!
    @IBOutlet weak var txtModelo: UITextField!
    @IBOutlet weak var txtMarca: UITextField!
    
    @IBAction func buscarAutomovil_TouchUpInside(_ sender: Any) {
        
    }
    
    @IBAction func guardarAutomovil_TouchUpInside(_ sender: Any) {
        let marca = txtMarca.text
        let modelo = txtModelo.text
        let año:Int32
        
        if marca != "" && modelo != "" && txtAño.text != ""{
            año = Int32(txtAño.text!)!
            var newAutomovil = Automoviles(context: managedObjectContext!)
            newAutomovil.anio = año
            newAutomovil.marca = marca
            newAutomovil.modelo = modelo
            
            do{
                try managedObjectContext?.save()
                limpiarDatos()
                print("Alumno guardado exitosamente")
            }
            catch{
                print("Error al tratar de guardar alumno")
            }
        }
    }
    
    func limpiarDatos(){
        txtAño.text = ""
        txtModelo.text = ""
        txtMarca.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inicializarCoreStack()
        lblStatus.text = lblTexto
        lblTexto = "Regresando"
    }

    func inicializarCoreStack()->Bool
    {
        var inicializado: Bool = false
        persistenContainer = NSPersistentContainer(name:"Automoviles")
        persistenContainer?.loadPersistentStores(completionHandler:{
            (storeDescription,error) in
            if error == nil{
                self.managedObjectContext = self.persistenContainer?.viewContext
                inicializado = true
            }
            else{
                print("Error")
                inicializado = false
            }
        })
        return inicializado
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ViewController{
            let aAVC = segue.destination as! ViewController
            aAVC.StatusMenu = txtMarca.text
        }
    }

}
