//
//  ViewController.swift
//  Automoviles
//
//  Created by CC5-29 on 08/06/18.
//  Copyright © 2018 CC5-29. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblStatusMenu: UILabel!
    var StatusMenu: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is AgregarAutomovilViewController{
            let aAVC = segue.destination as! AgregarAutomovilViewController
            aAVC.lblTexto = "Invocado desde menu"
        }
    }

    @IBAction func returned(segue:UIStoryboardSegue)
    {
        if segue.source is AgregarAutomovilViewController{
            let aAVC = segue.source as! AgregarAutomovilViewController
            notificacionDeUsuario("Regresando",aAVC.lblTexto!)
        }
    }

    func notificacionDeUsuario(_ etiqueta:String,_ mensaje:String)
    {
        lblStatusMenu.text = StatusMenu
    }
    
}

